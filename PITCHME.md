### @css[color-blue](Od przekładu przez prawo do... programowania)

##### @color[#3b56778c](Czyli jak oczarował mnie terminal i kolory w edytorze)

---?color=linear-gradient(90deg, #529cf4 65%, white 35%)
@title[O mnie]

@snap[north-west h4-white]
#### O mnie
@snapend

@snap[west span-45]
@ul[spaced text-white text-07]
- Junior Software Engineer w AirHelp
- Absolwentka kursu Junior Front-End Developer w infoShare Academy (9. edycja)
- Absolwentka Wydziału Filologii oraz Wydziału Prawa i Administracji UG
- Motocyklistka, koniara i fanka Wysp Kanaryjskich
@ulend
@snapend

@snap[east span-45]
@img[shadow](assets/img/paulina_natural.jpg)
@snapend

---

## @css[color-blue](Ale jak do tego doszło?)

---

@snap[north span-100]
### @css[color-blue](Dawno, dawno temu...)
@snapend

@snap[midpoint span-60]
![PIC](assets/img/brother.jpg)
@snapend

Note:
- Inspiracja od starszego brata
- Klasa humanistyczna i kompletny brak matematyki
- Studia - filologia angielska i administracja
- Tłumacz

---

@snap[north span-75]
### @css[color-blue](Humanistka.)
@snapend

@snap[midpoint span-60]
![PIC](assets/img/translator.jpg)
@snapend

Note:
- Humanistka i kropka. Nic się nie zmieni. O studiach informatycznych mogę pomarzyć, bo trzeba przecież znać matematykę.
- Spotykam sie z programistą - to mi wystarczy?
- Ach, ten terminal!
- Ale chwileczkę, poznaliśmy się na studiach. Można? Zaczynam marzyć.

---

@snap[north span-100]
### @css[color-blue](AirHelp i nowe możliwości)
@snapend
&nbsp;
@ul[spaced text-09 color-blue no-dots]
- Legal Assistant
- Junior Translation Specialist
- Praca z deweloperami
- Przykład innych
- Też tak chcę!
@ulend

Note:
- Do AirHelp przyszłam jako Legal Assistant
- Z działu prawnego do działu produktu
- Bliska wsprółpraca z deweloperami przez ponad rok - przypatrywanie się temu, co i jak robia, praca w Scrumie, bliskość kodu.
- Poznaję ludzi, którzy ukonczyli kurs.
- Też tak chcę!

---

@snap[north span-100]
### @css[color-blue](Czas rozpocząć przygodę!)
@snapend
&nbsp;
@snap[midpoint span-100]
@img[shadow](assets/img/adventure.jpg)
@snapend

---

@snap[north span-100]
### @css[color-blue](Kurs w infoShare Academy)
@snapend

@snap[west span-45]
@box[bg-green text-white rounded box-padding](Prework: przygotowanie i... weryfikacja)
@snapend

@snap[east span-45]
@box[bg-orange text-white rounded box-padding](Nauka, nauka i jeszcze raz nauka)
@snapend

@snap[south-east span-45]
@box[bg-pink text-white rounded box-padding](Praca w grupie)
@snapend

@snap[south-west span-45]
@box[bg-blue text-white rounded box-padding](Solidny start)
@snapend

Note:
- Prework - to nie tylko przygotowanie do zajęć, ale też weryfikacja naszych możliwosci. To jeszcze dobry moment na ewentualną rezygnację. Jeśli nie będziemy umieli pracować w trakcie preworku, to może nam po prostu nie zależy?
- Ciągła nauka - od teraz się dopiero zacznie.
- Praca w grupie - odzwierciedla rzeczywiste warunki w miejscu pracy.
- Solidny start - przygotowanie HR-owe, rozmowy próbne - dzięki temu bez problemu przeszłam rozmowę w AH. 

---

@snap[north-west span-60]
#### @css[color-blue](A po kursie...)
@snapend

@snap[west span-55]
@ul[spaced text-07 color-blue]
- Software Engineer Apprentice - 6 miesięcy
- Junior Software Engineer
- Czym się zajmuję?
    - Strona internetowa [AirHelp](https://www.airhelp.com/en/)
    - Strony w React generowane statycznie
    - Lambda w Go
    - Plus Code Review, dokumentowanie kodu, dzielenie się wiedzą, dbanie o proces scrumowy, rozmowy...
@ulend
@snapend

@snap[east span-40]
@img[shadow](assets/img/targi-pracy.jpg)
@snapend

Note:
- Apprentice przez 6 miesięcy
- Junior
- Rozmowy na regulara
- Co robię - strona internetowa
- React
- lambda w Golangu
- CR, dokumentowanie, Scrum...

---

@snap[north-west span-60]
#### @css[color-blue](O czym mi nie powiedzieli?)
@snapend

@snap[west span-55]
@ul[spaced text-07 color-blue]
- "Egzaminy"
- Nagły spadek przyrostu wiedzy
- To nie tylko kod!
- Wzloty i upadki
- Ciągłe rozwiązywanie problemów
- Code Review jako wspaniałe narzędzie do nauki
- Język angielski
@ulend
@snapend

@snap[east span-40]
@img[shadow](assets/img/hate-programming.jpg)
@snapend

Note:
- Rozmowy technologiczne są jak egzaminy. W rzeczywistości nikt nie odpytuje z teorii...
- Przyrost wiedzy nagle radykalnie spada - trzeba o niego dbać, nieustanna nauka.
- To nie tylko pisanie kodu, ale też praca z ludźmi, planowanie, biurokracja, dzielenie się wiedzą, odpowiedzialność za projekt.
- Ciągłe wzloty i upadki.
- Warto znać angielski.
- "Ja się do tego nie nadaję"?

---?color=#529cf4

@snap[midpoint span-100 text-white]
@quote[Ja się do tego nie nadaję](Senior Software Engineer)
@snapend
---

@snap[north span-100]
### @css[color-blue](Co jest wspaniałego w programowaniu?)
@snapend
&nbsp;
@ul[spaced text-09 color-blue]
- Proces twórczy
- Możliwość weryfikacji naszej pracy
- Ludzie @fa[heart fa-xs]
- Nauka
- Kolory @fa[smile fa-xs]
@ulend

Note:
- Tworzymy coś działającego i istniejącego, namacalnego. Po przygodzie humanistycznej to miła odmiana.
- Możliwosc weryfikacji kodu.
- Wspaniali ludzie - nie tylko deweloperzy, ale też testerzy, desginerzy, scrum masterzy, produktowcy...
- Ciągła nauka - błogosławieństwo i przekleństwo.
- Kolory -> slajd.

---

@img[shadow](assets/img/vsc.png)

---

@snap[north span-100]
### @css[color-blue](Odwagi!)
@snapend
&nbsp;
@snap[midpoint span-60]
@img[shadow](assets/img/nedstark.jpg)
@snapend

Note:
- Przygotujcie się na zmianę życia (ale czy nie o to chodzi?)
- Determinacja - pomaga w nauce i potem w znalezieniu pracy

---

### @css[color-blue](@fa[linkedin])
### @css[color-blue][Paulina Chojnowska](https://www.linkedin.com/in/paulina-chojnowska/)

Note:
Zachęcam do kontaktu